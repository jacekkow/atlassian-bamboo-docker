#!/bin/bash

set -e

if [[ -d ${BAMBOO_HOME}/overlay ]]
then
	cp -afv ${BAMBOO_HOME}/overlay/. ${BAMBOO_INSTALL}/
fi

/bin/bash -c "${BAMBOO_INSTALL}/bin/start-bamboo.sh -fg"
