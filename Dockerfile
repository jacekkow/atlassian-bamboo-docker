FROM java:8-jre
MAINTAINER Mateusz Małek <mmalek@iisg.agh.edu.pl>

ENV BAMBOO_VERSION 5.9.7
ENV BAMBOO_HOME /opt/atlassian/bamboo/home
ENV BAMBOO_INSTALL /opt/atlassian/bamboo/install

ADD ./jessie-backports.list /etc/apt/sources.list.d/jessie-backports.list
RUN apt-get update -qq && \
	apt-get install -y --no-install-recommends libfontconfig1 git openssh-client && \
	apt-get clean autoclean && \
	apt-get autoremove -y && \
	mkdir -p $BAMBOO_INSTALL && \
	wget https://downloads.atlassian.com/software/bamboo/downloads/atlassian-bamboo-${BAMBOO_VERSION}.tar.gz -O- | \
	tar xz --strip=1 -C $BAMBOO_INSTALL && \
	useradd --home-dir $BAMBOO_HOME --shell /bin/bash --create-home bamboo && \
	chown -R bamboo:bamboo $BAMBOO_INSTALL

RUN echo "bamboo.home = $BAMBOO_HOME" > ${BAMBOO_INSTALL}/atlassian-bamboo/WEB-INF/classes/bamboo-init.properties

VOLUME ["$BAMBOO_HOME"]

EXPOSE 8085

ADD ./entrypoint.sh /entrypoint.sh

WORKDIR $BAMBOO_HOME
USER bamboo:bamboo

ENTRYPOINT ["/bin/bash", "/entrypoint.sh"]
